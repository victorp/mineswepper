package com.viktordev.app;

import lombok.experimental.UtilityClass;

import java.util.Random;

@UtilityClass
public class BoardUtil {

    public static void initBoard(Cell[][] board, int bombsAmount) {
        initEmptyBoard(board);
        initBombs(board, bombsAmount);
        setValues(board);
    }

    public static void printBoard(Cell[][] board){
        System.out.print("\t ");
        for(int i=0; i<9; i++)
        {
            System.out.print(" " + i + "  ");
        }
        System.out.print("\n");
        for(int i=0; i<9; i++)
        {
            System.out.print(i + "\t| ");
            for(int j=0; j<9; j++)
            {
                System.out.print(board[i][j]);
                System.out.print(" | ");
            }
            System.out.print("\n");
        }
    }

    public boolean isValid(int i, int j){
        return (0 <= i && i < 9) && (0 <= j && j < 9);
    }

    private static void initEmptyBoard(Cell[][] board) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                board[i][j] = new Cell();
            }
        }
    }

    private static void initBombs(Cell[][] board, int bombsAmount) {
        int count = 0;
        int i;
        int j;
        Random random = new Random();
        while (count != bombsAmount){
            i = random.nextInt(9);
            j = random.nextInt(9);
            if (-1 != board[i][j].getValue()){
                board[i][j].setValue(-1);
                count++;
            }
        }
    }

    private static void setValues(Cell[][] board) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (!board[i][j].isBomb()){
                    board[i][j].setValue(countValue(board, i, j));
                }
            }
        }
    }

    private static int countValue(Cell[][] board, int i, int j){
        int value = 0;
        for (int k = i-1; k <= i+1; k++) {
            for (int l = j-1; l <= j+1; l++) {
                if (isValid(k, l) && !(k == i && l == j )){
                    if (board[k][l].isBomb()) value++;
                }
            }
        }
        return value;
    }


}
