package com.viktordev.app;

import java.util.Scanner;

import static com.viktordev.app.BoardUtil.*;

public class MineSweeper {
    private static final int SIZE = 9;
    private static final int MAX_OPENED_CELLS = 71;
    private static final Integer BOMBS_COUNT = 10;
    private static final String ERROR_MSG = "Pay attention, plz";
    private final Cell[][] board;
    private final Scanner scanner;
    private int openedCells;
    private int markedCells;
    private boolean gameOver;



    public MineSweeper() {
        board = new Cell[SIZE][SIZE];
        scanner = new Scanner(System.in);
        gameOver = false;
        initBoard(board, BOMBS_COUNT);
    }

    public void play(){
        printBoard(board);

        while (!gameOver){
            try {
                System.out.format("make your move, plz, you have %d bomb(s) left\n", BOMBS_COUNT - markedCells);
                String input = scanner.nextLine();
                String[] parts = input.split(",");
                gameOver = makeMove(parts[0].charAt(0), Integer.parseInt(parts[1]), Integer.parseInt(parts[2]));
                printBoard(board);
                checkFinish();
            }
            catch (Exception e){
                System.out.println(ERROR_MSG);
            }
        }
    }

    private boolean makeMove(char operation, int i, int j){
        if (operation == 'm' && isValid(i, j) && !board[i][j].isMarked()){
            board[i][j].setMarked(true);
            markedCells++;
            return false;
        }
        if (operation == 'u' && isValid(i, j) && board[i][j].isMarked()){
            board[i][j].setMarked(false);
            markedCells--;
            return false;
        }
        if (operation == 'o' && isValid(i, j) && !board[i][j].isVisible()){
            return openCell(i, j);
        }
        System.out.println(ERROR_MSG);
        return false;
    }

    private boolean openCell(int i, int j){
        if (board[i][j].isBomb()){
            board[i][j].setVisible(true);
            return true;
        }
        else {
            openSimpleCell(i, j);
            return false;
        }
    }

    private void openNeighbors(int i, int j){
        for (int k = i-1; k <= i+1; k++) {
            for (int l = j-1; l <= j+1; l++) {
                if (isValid(k, l) && !(k == i && l == j ) && !board[k][l].isVisible()){
                    openSimpleCell(k, l);
                }
            }
        }
    }

    private void openSimpleCell(int i, int j){
        board[i][j].setVisible(true);
        openedCells++;
        if (board[i][j].getValue() == 0){
            openNeighbors(i, j);
        }
    }

    private void checkFinish(){
        if (gameOver){
            System.out.println("You lose.");
        }
        else {
            if (openedCells == MAX_OPENED_CELLS){
                System.out.println("You win.");
                gameOver = true;
            }
        }
    }
}
