package com.viktordev.app;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cell {
    private static final String HIDDEN = "?";
    private static final String MARKED = "#";
    private static final String BOMB = "*";
    private int value; // -1 for bomb
    private boolean visible;
    private boolean marked;

    @Override
    public String toString() {
        if (visible){
            return isBomb() ? BOMB : String.valueOf(value);
        }
        else return marked ? MARKED : HIDDEN;
    }

    public boolean isBomb(){
        return value == -1;
    }
}
