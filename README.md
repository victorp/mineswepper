Use Java (version > 11) to run `java -jar mineswepper.jar`
 - For mark predicted bomb: `m,<rowNumber, colNumber>` e.g. `m,0,6`.
 - For unmark predicted bomb: `u,<rowNumber, colNumber>` e.g. `u,0,6`.
 - For open cell: `o,<rowNumber, colNumber>` e.g. `o,0,6`.